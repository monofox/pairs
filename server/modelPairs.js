var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var PairsCardSchema=new Schema({
	image1: {type: String, required:false},
	text1:	{type: String, required:false, max: 300},
	image2: {type: String, required:false},
	text2:	{type: String, required:false, max: 300}
})

var PairsSchema=new Schema({
	title :		{type: String, required: true, max: 300},
	license:	{type: String, required: false, max: 1000},
	desc :		{type: String, max: 300},
	owner:		{type: String, max: 300},
	subject:	{type: String, max: 300, default: ""},
	ticket:		{type: String, max: 300, default: ""},
	visible:	{type: Boolean, default: false},
	background:	{type: String, default: null },
	cards:	[PairsCardSchema]
});

module.exports=mongoose.model('Pairs', PairsSchema);
