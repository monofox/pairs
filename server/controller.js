//External libs
const { check,body,param,validationResult }=require('express-validator/check');
const { sanitize }=require('express-validator/filter');
const debug=require('debug')('pairs');
const cron=require('node-cron');
const generator=require('../../../main/server/passwordHelper');
const path=require('path');
const fs=require('fs');
const WebSocket=require('ws');

//Internal libs
const consts=require ('../../../consts.js');
const config=__config;
const mainController=require('../../../main/server/controller');
const userHelper=require('../../../main/server/userHelper');

//database
var Pairs=require('./modelPairs');

const WS_PING_INTERVAL=100;
const TICKET_LENGTH=10;	//default value when generating new tickets

/********************************************************************************
 Open functions
 ********************************************************************************/

/*
Return meta information on available games
- Authentication via checkAuth (route)
*/
exports.getSetsMeta=function(req, res, next) {
	var login=res.locals.user ? res.locals.user.login : '';
	Pairs.find({ $or:[ {owner:login}, {visible:true}]},'title desc owner subject ticket visible').sort({ subject: 'asc', title: 'asc'}).exec(function(err,allAbcdSets) {
		res.json(allAbcdSets);
	});
}

/*
Get set by topic and name
- Authentication via checkAuth (route)
*/
exports.getSet=function(req, res, next) {
	var pairs=res.locals.pairs;
	if (!pairs) return res.json([{msg:'forbidden'}]);
	res.json(pairs);
}

//Load quiz from ticket in request - middleware
exports.loadSetFromTicket=[
	sanitize('ticket').trim().escape(),
	(req, res, next)=> {
		var ticket=req.query.ticket || req.params.ticket || req.body.ticket;
		Pairs.findOne({ticket:ticket}, function(err,pairs) {
			if (err) debug(err);
			if (!pairs) return res.json([{ msg: 'forbidden'}]);
			res.locals.pairs=pairs;
			next();
		});
	}
]

//return socket url
exports.getSocketUrl=[
	sanitize('ticket'),
	(req,res,next)=> {
		var url=(config.URL_PROTO=='http://' ? 'ws://' : 'wss://')+path.join(config.URL_HOST,config.URL_PREFIX,'pairs','ws',req.params.ticket);
		res.json({url:url});
	}
]

/********************************************************************************
 Author functions
 ********************************************************************************/

/*
 Check authorization
 */
exports.checkAuth=function(req, res, next) {
   mainController.checkAuthAndRole(req,res,next,"pairs");
}

/*
 Load quiz from id in request - middleware
 */
exports.loadSetFromId=[
	sanitize('id').trim().escape(),
	(req, res, next)=> {
		var login=res.locals.user ? res.locals.user.login : '';
		var id=req.query.id || req.params.id || req.body.id;
		Pairs.findById(id, function(err,pairs) {
			if (err) debug(err);
			if (!pairs) return res.json([{msg: 'forbidden'}]);
			if (!pairs.visible && login!=pairs.owner) return res.json([{msg: 'forbidden'}]);
			res.locals.pairs=pairs;
			next();
		});
	}
]

/*
 Make sure that the logged-in user is allowed to edit the current quiz - middleware
 */
exports.ensureUserIsOwner=function(req,res,next) {
	if (res.locals.pairs.owner !=res.locals.user.login)
		return res.json([{msg:'forbidden'}]);
	next();
}

/*
 Import Json format (new or update)
 - Authentication via checkAuth (route)
 - no further authorization (ever author may import sets)
 */
exports.importJson=function(req,res,next) {
	var data=req.body.pairs;
	if (!data || !data.title) return res.json([{msg:'no-file-selected'}]);
	//Validate meta-data
	var errors=[];
	var parsedData=parseJson(data,errors);
	parsedData.owner=res.locals.user.login;
	//New entry - no id
	if (!data._id) {
		var pairs=new Pairs (parsedData);
		//Verify that there is no game with the same ticket
	   	Pairs.findOne({ticket:pairs.ticket}, function(err,pairsWithSameTicket) {
			if (err) debug(err);
			if (pairsWithSameTicket) pairs.ticket=generator.generate({length: TICKET_LENGTH, numbers: true});
			//Save
			pairs.save(
			   function(err) {
				   if (err) {
					   debug(err);
					   errors=[{msg:'database-error'}];
				   }
				   res.json(errors);
			   }
		   )
	   })
	}
	//Update existing entry
	else {
		Pairs.updateOne({_id:data._id},{ $set: parsedData },  function(err) {
		   if (err) {
			   debug(err);
			   errors=[{msg:'database-error'}];
		   }
		   res.json(errors);
	   })
	}
}

/*
 Delete existing set
 */
exports.deleteSet=function(req, res,next) {
	var pairs=res.locals.pairs;
    Pairs.deleteOne({_id:pairs._id}, function(err) {
 	   if (err) {
 		   debug(err);
 		   return res.json('database-error');
 	   }
 	   return res.json({});
    })
}

/********************************************************************************
 Utils
 ********************************************************************************/

function parseJson(data,errors) {
	//Validate meta-data
   	var pairs={
		title:		(data.title && data.title.match(consts.REGEX_TEXT_300)) ? data.title : '–',
		desc:		(data.desc && data.desc.match(consts.REGEX_TEXT_300)) ? data.desc : '',
		license:	(data.license && data.license.match(consts.REGEX_TEXT_AREA_1000)) ? data.license : '',
		subject:	(data.subject && data.subject.match(consts.REGEX_TEXT_300)) ? data.subject : '',
		ticket:		(data.ticket && data.ticket.match(consts.REGEX_TICKET)) ? data.ticket : generator.generate({length: TICKET_LENGTH, numbers: true}),
		visible:	(data.visible=='true' ? true : false),
		background:	(data.background && data.background.match(consts.REGEX_DATA_URI) && data.background.length<consts.MAX_INLINE_IMAGE_SIZE*1.4 ? data.background : ''),
		cards:		[]
	};
   	//Card pairs
   	if (data.cards && data.cards.length>0) {
	   	for (dq of data.cards) {
		   	card={
				image1: (dq.image1 && dq.image1.match(consts.REGEX_DATA_URI) && dq.image1.length<consts.MAX_INLINE_IMAGE_SIZE*1.4 ? dq.image1 : '' ),
				image2: (dq.image2 && dq.image2.match(consts.REGEX_DATA_URI) && dq.image2.length<consts.MAX_INLINE_IMAGE_SIZE*1.4 ? dq.image2 : '' ),
				text1: (dq.text1 && dq.text1.match(consts.REGEX_TEXT_300)) ? dq.text1 : '',
				text2: (dq.text2 && dq.text2.match(consts.REGEX_TEXT_300)) ? dq.text2 : '',
		   	}
		   	if (pairs.cards.length<25) pairs.cards.push(card);
	   	}
   	}
	return pairs;
}

/*
Delete all user data - only used from user controller
*/
exports.deleteUserData=function(login) {
	return new Promise(resolve=> {
		Pairs.deleteMany({owner:login}, function(err) {
			if (err) debug(err);
	 		resolve();
	    })
	})
}


/********************************************************************************
 Websocket
 ********************************************************************************/

//List of all websocket clients
var pairsSockets={};

/*
 Register new client
*/
exports.initWs=function(ws, req) {
	var instance=req.params.instance;
	if (!instance.match(consts.REGEX_TICKET)) {
		debug('Ignored WS request, validation error instance.');
		return;
	}
	//add socket
	if (!pairsSockets[instance]) pairsSockets[instance]=new Set();
	pairsSockets[instance].add({ws:ws});
	ws.on('pong', function() {
		this.isAlive=true;
	});
	ws.on('message', function(json) {
		var msg={};
		var send={};
		try {
			var msg=JSON.parse(json);
			if (msg.type=='open') {
				if (!Number.isInteger(msg.pair)) return;
				if (!Number.isInteger(msg.num)) return;
				if (!msg.clientId || !msg.clientId.match(consts.REGEX_TICKET)) return;
				debug("Open card, instance: "+instance);
				broadcastMessage(instance,{type:'open',pair:msg.pair,num:msg.num,clientId:msg.clientId});
			}
			if (msg.type=='retry') {
				if (!msg.clientId || !msg.clientId.match(consts.REGEX_TICKET)) return;
				debug("Open card, instance: "+instance);
				broadcastMessage(instance,{type:'retry',clientId:msg.clientId});
			}
		}
		catch (e) {
			debug(e);
		};
	})
	broadcastMessage(instance, {type:'numPlayers',num:pairsSockets[instance].size})
	debug("New WS chat client for instance "+instance);
}


/*
 Broadcast message
*/
function broadcastMessage(instance,message) {
	var json=JSON.stringify(message);
	var socketSet=pairsSockets[instance];
	if (!socketSet) return;
	socketSet.forEach(function(socket) {
		if (socket.ws.readyState==WebSocket.OPEN) {
			try {socket.ws.send(json)} catch (e) {debug(e);}
		}
	});
}


/*
Send ping to all websockets
 - to check if they are still connected
 - Ensure that the reverse proxy does not break the connection
*/
setInterval(function () {
	var count=0;
	for (instance in pairsSockets) {
		socketSet=pairsSockets[instance];
		socketSet.forEach(function(socket) {
			if (socket.ws.isAlive===false || socket.ws.readyState!=WebSocket.OPEN) {
				debug("WS chat client closed for instance: "+instance);
				socketSet.delete(socket);
				try {socket.ws.close();} catch (e) {debug(e);}
				broadcastMessage(instance, {type:'numPlayers',num:socketSet.size})
			}
			else {
				count++;
				socket.ws.isAlive=false;
				try { socket.ws.ping();} catch (e) {debug (e);}
			}
		})
	}
	debug("Number of open ws sockets: "+count)
}, WS_PING_INTERVAL*1000);
