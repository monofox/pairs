
import Icon from './img/pairs.svg';

export default {
	id: "pairs",
	meta: {
		title: 	{
			"de": "Online Pairs",
			"en": "Image Pairs"
		},
		text:	{
			"de": "Pairs Spiel gemeinsam mit Anderen online spielen",
			"en": "Search matching images online with others"
		},
		to: 	"pairs-index",
		icon: 	Icon,
		role: 	"pairs",
		index: true
	},
	routes: [
		{	path: '/pairs-index', name:'pairs-index', component: () => import('./views/Index.vue') },
		{	path: '/pairs-edit', name:'pairs-edit', component: () => import('./views/Edit.vue') },
		{	path: '/pairs-game/:ticket', name:'pairs-game', component: () => import('./views/Game.vue') },
		{	path: '/pairs-game/:ticket/:instance', name:'pairs-game-instance', component: () => import('./views/Game.vue') }
	],
	imageMaxSize: 0.1*1024*1204,
	maxNumCards: 25
}
