# Pairs

Kurze Einführung{.subtitle}

## Starten eines Spiels

Pairs ermöglicht es, gemeinsam online **ein** Spiel mit beliebig vielen Mitspielern zu spielen.

Der *erste Spieler* nutzt dazu den **Startlink**, der in der Übersicht der Spiele mittels der Share-Funktion verfügbar ist:

![Übersicht der Spiele für angemeldete Personen](text/de/pairs/index.png){.center}

Ruft der *erste Spieler* diesen Einstiegslink auf, so kann er (solange noch keine Züge durchgeführt wurden) weitere Mitspieler über einen **Einladungslink** einladen:

![Übersicht der Spiele für angemeldete Personen](text/de/pairs/einladungslink.png){.center}

## Regeln

Die App gibt nur wenige elementare Regeln vor:

1. Es wird immer zuerst eine Karte aufgedeckt, dann eine zweite.
2. Gehören die Karten zusammen, so erhält der Spieler, der die zweite aufgedeckt hat einen Punkt.

Über alle weiteren Regeln, die Reihenfolge etc. müssen sich die Spieler selbst einigen, so sind z.B. auch völlig neue Spielvarianten im Team denkbar.

## Neues Spiel anlegen

Um ein neues Spiel anlegen zu können, müssen Sie auf dieser Seite registriert sein. Bei Fragen wenden Sie sich bitte an den im [Impressum](#/text-main-imprint) genannten Betreiber der Seite.
